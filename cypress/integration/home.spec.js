/// <reference types="cypress" />

describe('homepage is loaded', function() {
    it('checks that elements are displayed', function() {
      cy.visit('/')
      cy.get('main > h1').contains('Hi people')
      cy.get('picture > img').should('be.visible')
      cy.get('main > a').click()
      cy.url().should('contain', '/page-2/')
    })
  })